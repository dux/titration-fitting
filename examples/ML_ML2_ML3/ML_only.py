# -*- coding: utf-8 -*-
import sys
sys.path.append('..')

from glob import glob

# use numpy to load the content of the textfiles to arrays
import numpy as np 
from scipy.interpolate import interp1d
# import the needed classes
from simulateSpectrum import Component, System, Titration

# %% 
"""
LOADING AND SAMPLING THE DATA
"""
folder = '../data/'
## ligand/metal ratios at the different points of the titration:
eqs = np.loadtxt(folder+'eqs.csv')
#
beg=30; end=120; every=2
## span of the UV-VIS spectrum: 
span = np.loadtxt(folder+'span.csv')[::-1][beg:end:every]
# spanold = np.loadtxt(folder+'span.csv')[::-1][30:150:1]
spanold = np.loadtxt(folder+'span.csv')[::-1][30:120:2]
#
## values of the UV-VIS spectrum: (contained by columns in the file)
uvs  = np.loadtxt(folder+'uvs.csv')[beg:end:every,:]

volumes = np.loadtxt(folder+'volumes.csv')

def interpolate(x, y, xnew):
    f = interp1d(x, y, kind='cubic', bounds_error=False, fill_value='extrapolate')
    return f(xnew)

#%% 
"""
CREATING THE SPECIES, ARRANGING THEM IN A SYSTEM, INITIALIAZING THE TITRATION
"""
# constructor of a component :
# Component( guess for equilibrium constant, initial concentration, {named arguments})
# (see docstring)
# if the Component is a building-block (M or L), the value of the equilibrium constant
# does not matter. (below, 0 is given as the initial guess.)

eqknown = 0
uvknown = 1

conc_ini = 0.02
# UNITS IN MMOL, MULTIPLY BY 1000, 1000^2, 1000^3 FOR ML, ML2, ML3

M    = Component(0,    0.,  uv_known=True,  uvvis=np.zeros_like(span), eqconst_known=True, \
                 name='M', titrant=True) # the metal is the titrant.

L    = Component(0,    conc_ini,  uv_known=True,  uvvis=uvs[:,0]/conc_ini, eqconst_known=True,\
                 name='L') 

ML   = Component(10**4.76,  0.,  buildblocks=[M,L],  coeffs=[1,1], uv_known=uvknown,  
                 uvvis=interpolate(spanold, np.load('ML_uvvis.npy'), span),
                 eqconst_known=eqknown)

S    = System([M,L], [ML], conc_ini=conc_ini, span=span)

#      Titration(system, M/L ratios, experimental uv-vis spectra)
beg  = 0
end  = 12
T    = Titration(S, eqs[beg:end], uvs[:, beg:end], volumes=volumes[beg:end])
#%%
# OPTIMIZE 
T.optimize()
# plot and print the result
T.plotCurrentModel()
T.printCurrentModel()
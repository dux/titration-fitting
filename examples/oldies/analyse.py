# -*- coding: utf-8 -*-


from glob import glob

# use numpy to load the content of the textfiles to arrays
import numpy as np 
from scipy.interpolate import interp1d
# import the needed classes
from simulateSpectrum import Component, System, Titration

# %% 
"""
LOADING AND SAMPLING THE DATA
"""
#import matplotlib.pyplot as plt
#
#def loadspectrum(file):
#    wav, A = np.loadtxt(file, skiprows=3, delimiter=',', usecols=(0,1), unpack=1)
#    return wav, A
#
#eqs = []
#mat = []
#for file in sorted(glob('data/*csv')):
#    if not ('blank' in file or 'T3' in file):
#        wav, A = loadspectrum(file)
#        eq = float(file.replace('data/', '').replace('.csv',''))
#        eqs.append(eq)
#        mat.append(A)
#        print(len(mat), eq)
#        print(eqs)
#matsave = mat.copy()
#eqs, mat = np.array(eqs), np.array(mat)

#np.savetxt('eqs.csv', eqs)
#np.savetxt('uvs.csv', mat)
#np.savetxt('span.csv', wav)
folder = 'data/'
## ligand/metal ratios at the different points of the titration:
eqs = np.loadtxt(folder+'eqs.csv')
#
beg=30; end=150; every=2
## span of the UV-VIS spectrum: 
span = np.loadtxt(folder+'span.csv')[::-1][beg:end:every]
spanold = np.loadtxt(folder+'span.csv')[::-1][30:150:1]
#
## values of the UV-VIS spectrum: (contained by columns in the file)
uvs  = np.loadtxt(folder+'uvs.csv')[beg:end:every,:]

volumes = np.loadtxt(folder+'volumes.csv')

def interpolate(x, y, xnew):
    f = interp1d(x, y, kind='cubic', bounds_error=False, fill_value='extrapolate')
    return f(xnew)

#%% 
"""
CREATING THE SPECIES, ARRANGING THEM IN A SYSTEM, INITIALIAZING THE TITRATION
"""
# constructor of a component :
# Component( guess for equilibrium constant, initial concentration, {named arguments})
# (see docstring)
# if the Component is a building-block (M or L), the value of the equilibrium constant
# does not matter. (below, 0 is given as the initial guess.)

eqknown = 0
uvknown = 1


M    = Component(0,    0.,  uv_known=True,  uvvis=np.zeros_like(span), eqconst_known=True, \
                 name='M', titrant=True) # the metal is the titrant.

L    = Component(0,    1.,  uv_known=True,  uvvis=uvs[:,0], eqconst_known=True,\
                 name='L') 

ML   = Component(1028,  0.,  buildblocks=[M,L],  coeffs=[1,1], uv_known=uvknown,  uvvis=interpolate(spanold, np.load('ML_uvvis.npy'), span),#uvs[:,-1], 
                 eqconst_known=eqknown)
ML2  = Component(100  , 0.,  buildblocks=[M,L], coeffs=[1,2], uv_known=uvknown,eqconst_known=eqknown, uvvis=uvs[:, 0]*1 + uvs[:,-1]*1)
ML3  = Component(70804, 0.,  buildblocks=[M,L], coeffs=[1,3], uv_known=uvknown, eqconst_known=eqknown, uvvis=interpolate(spanold, np.load('ML3_uvvis.npy'), span))
ML4  = Component(100, 0.,  buildblocks=[M,L], coeffs=[1,4], uv_known=uvknown, eqconst_known=eqknown, uvvis=uvs[:, 0]*3 + uvs[:,-1]*1)
ML5  = Component(12.8e6, 0.,  buildblocks=[M,L], coeffs=[1,5], uv_known=uvknown, eqconst_known=eqknown, uvvis=interpolate(spanold, np.load('ML5_uvvis.npy'), span))#uvvis=uvs[:, 0]*4 + uvs[:,-1]*1)
#ML2  = Component(1000,  0.,  buildblocks=[M,L],  coeffs=[1,2], uv_known=False,  uvvis=uvs[:,-1], 
#                 eqconst_known=False)
#      System(list of building blocks, list of components, span of the uv-vis spectra)
S    = System([M,L], [ML, ML3, ML5], span=span)

#      Titration(system, M/L ratios, experimental uv-vis spectra)
beg  = 0
end  = 13
T    = Titration(S, eqs[beg:end], uvs[:, beg:end], volumes=volumes[beg:end])#, volumes[beg:end])

# OPTIMIZE 
# T.optimize()
# plot and print the result
T.plotCurrentModel()
T.printCurrentModel()
